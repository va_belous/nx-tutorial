# [1.2.0](https://gitlab.com/va_belous/nx-tutorial/compare/ui-kit-v1.1.0...ui-kit-v1.2.0) (2023-10-15)


### Features

* Update ([9ebac71](https://gitlab.com/va_belous/nx-tutorial/commit/9ebac7112d33a6069d9d82e06a6f3ad954f14073))

# [1.1.0](https://gitlab.com/va_belous/nx-tutorial/compare/ui-kit-v1.0.2...ui-kit-v1.1.0) (2023-10-14)


### Features

* Update ([9a6bf3e](https://gitlab.com/va_belous/nx-tutorial/commit/9a6bf3e57d6d03286486f6befa36dce9fdf17d19))

## [1.0.2](https://gitlab.com/va_belous/nx-tutorial/compare/ui-kit-v1.0.1...ui-kit-v1.0.2) (2023-10-14)


### Bug Fixes

* Update ([62ef2d8](https://gitlab.com/va_belous/nx-tutorial/commit/62ef2d85b4343ed2e72c0b5134e8145b9382b545))
* Update ([7501162](https://gitlab.com/va_belous/nx-tutorial/commit/7501162c42435be8ef74d70eae97a1bc6e336ca6))

## [1.0.1](https://gitlab.com/va_belous/nx-tutorial/compare/ui-kit-v1.0.0...ui-kit-v1.0.1) (2023-10-14)


### Bug Fixes

* Update ([7961c86](https://gitlab.com/va_belous/nx-tutorial/commit/7961c86067c7a85fe1f3efb4244038508a9e0d6f))
* Update ([366e4d8](https://gitlab.com/va_belous/nx-tutorial/commit/366e4d822ea886bea311ab74dd79264d6cd80d6c))

# 1.0.0 (2023-10-14)


### Bug Fixes

* Add component ([0b0144a](https://gitlab.com/va_belous/nx-tutorial/commit/0b0144a440f2605edeeb1dc4ba45903cb93c52e9))


### Features

* Create ui-kit ([6e1234b](https://gitlab.com/va_belous/nx-tutorial/commit/6e1234b931b21b716f0b35405b12da3aaee361d4))
