import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TComponent } from './t/t.component';

@NgModule({
  imports: [CommonModule],
  declarations: [TComponent],
  exports: [TComponent],
})
export class UiKitModule {}
