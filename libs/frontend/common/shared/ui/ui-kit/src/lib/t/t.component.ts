import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'nx-tutorial-t',
  templateUrl: './t.component.html',
  styleUrls: ['./t.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TComponent {}
